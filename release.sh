#!/bin/bash
if [[ $# -eq 0 ]] ; then
    echo 'Ingrese al menos un directorio para actualizar'
    exit 1
fi
for var in "$@"
do
  echo "Se actualizará $var";

  cd ~;
  # # Entrar al directorio
  cd /var/www/html/$var;
  #
  # # # Pull ultimos cambios
  git fetch;
  git tag -d $(git tag) 
  git checkout master 
  git fetch --tags  
  latesttag=$(git describe --tags)
  git checkout ${latesttag}
  echo "Se actualizó a version $latesttag"
  ./protected/yiic migrate --interactive=0
done
