#!/bin/bash
if [[ $# -ne 2 ]] ; then
    echo 'Ingrese un directorio y una branch para actualizar'
    exit 1
fi
sitio=$1;
branch=$2
  echo "$sitio";
  # Home
  cd ~;
  # # Entrar al directorio
  cd /var/www/html/$sitio;
  #
  # # # Pull ultimos cambios
  git fetch --all
  git checkout $branch;
  git pull
  #sudo sed -i "s/\/\/'afterLoginUrl'=>/'afterLoginUrl'=>array('\/'),\/\//" protected/config/main.php
  ./protected/yiic migrate --interactive=0
