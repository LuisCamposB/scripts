#!/bin/bash
if [[ $# -ne 4 ]] ; then
    echo 'Debe ingresar el sitio base, el sitio destino, el dominio y el nombre del proyecto git. por ejemplo\n
xticket xticket_2 gestiondeaccesos.com boletera.cityticket'
    exit 1
fi

sitio1=$1 
sitio2=$2 
dominio=$3 
proyecto=$4 

echo $sitio1
echo $sitio2
echo $dominio
echo $proyecto

cd /etc/apache2/sites-available
sudo cp $sitio1.$dominio.conf $sitio2.$dominio.conf
sudo sed -i "s/$sitio1/$sitio2/" $sitio2.$dominio.conf
cd ../sites-enabled/
sudo ln -s ../sites-available/$sitio2.$dominio.conf
git clone git@bitbucket.org:tecnocen/$proyecto.git /var/www/html/$sitio2
cd /var/www/html/$sitio2
latesttag=$(git describe --tags)
git checkout ${latesttag}
sudo a2ensite $sitio2.$dominio.conf
sudo service apache2 restart

